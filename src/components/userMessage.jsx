import React from "react";

const UserMessage = ({ selectedUser }) => {
  return (
    <div className="user-message bubble">
      <em>{selectedUser.name}</em> - {selectedUser.message}
    </div>
  );
};

export default UserMessage;
