import React from "react";

const Buttons = ({ selectedItem, onClockInOut, onLocationChange }) => {
  const { clockedIn, inOffice } = selectedItem;

  return (
    <div className="buttons">
      <input
        type="button"
        value={clockedIn ? "Clock Out" : "Clock In"}
        onClick={onClockInOut}
        disabled={!selectedItem}
        className="button"
      ></input>
      <input
        type="button"
        value={inOffice ? "Outside Office" : "In Office"}
        onClick={onLocationChange}
        disabled={!selectedItem}
        className="button"
      ></input>
    </div>
  );
};

export default Buttons;
