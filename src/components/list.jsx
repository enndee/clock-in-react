import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { faCommentAlt } from "@fortawesome/free-regular-svg-icons";

const List = ({ items, selectedItem, onItemSelect }) => {
  return (
    <form className="list">
      {items.map(item => (
        <div
          key={item.id}
          className={`${item.id === selectedItem.id ? "item selected " : "item "}
          ${item.clockedIn ? "clocked-in " : "clocked-out "} 
          ${item.inOffice ? "in-office" : "outside-office"}`}
          onClick={() => onItemSelect(item)}
        >
          <input type="radio" id={item.id} value={item.id} name="people" onClick={() => onItemSelect(item)} />
          <label className="item-name" htmlFor={item.id} onClick={() => onItemSelect(item)}>
            {item.name}
            {!item.inOffice && <span className="screen-reader-text"> is working outside the office</span>}
            {item.message && <span className="screen-reader-text"> says: {item.message}</span>}
          </label>
          <div className="item-logos">
            <div className="outside-office-logo">
              {!item.inOffice && (
                <React.Fragment>
                  <FontAwesomeIcon icon={faHome} />
                  <span className="screen-reader-text"> is working outside the office</span>
                </React.Fragment>
              )}
            </div>
            <div className="message-logo">
              {item.message && (
                <React.Fragment>
                  <FontAwesomeIcon icon={faCommentAlt} />{" "}
                  <span className="screen-reader-text"> says: {item.message}</span>
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      ))}
    </form>
  );
};

export default List;
