import React from "react";

const Loading = () => {
  return (
    <div className="loading">
      <div className="loading-text">Loading</div>
      <div className="loading-circle">
        <div className="hours"></div>
        <div className="minutes"></div>
      </div>
    </div>
  );
};

export default Loading;
