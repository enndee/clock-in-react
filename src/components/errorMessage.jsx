import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";

const ErrorMessage = ({ errorMessage }) => {
  return (
    <div className="error">
      <FontAwesomeIcon icon={faExclamationTriangle} size="lg" />
      <span className="error-message" role="alert">
        {errorMessage}
      </span>
      <FontAwesomeIcon icon={faExclamationTriangle} size="lg" />
    </div>
  );
};

export default ErrorMessage;
