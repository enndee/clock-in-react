import React from "react";
import Input from "./input";

const messageInput = ({ selectedUser, value, onMessageFieldChange, onSubmit }) => {
  return (
    <React.Fragment>
      <div className="message-input">
        <Input
          labelText="Leave a message: "
          submitButtonText="Submit message"
          inputId="message-textbox"
          value={value}
          onChange={onMessageFieldChange}
          onSubmit={onSubmit}
          itemSelected={selectedUser}
        />
      </div>
    </React.Fragment>
  );
};

export default messageInput;
