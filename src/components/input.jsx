import React from "react";

const Input = ({ labelText, submitButtonText, inputId, onChange, onSubmit, value, itemSelected }) => {
  return (
    <form onSubmit={onSubmit}>
      <label htmlFor={inputId}>{labelText}</label>
      <input
        type="text"
        id={inputId}
        value={value}
        onChange={e => onChange(e.currentTarget.value)}
        disabled={!itemSelected}
      />
      <input type="submit" value={submitButtonText} disabled={!itemSelected}></input>
    </form>
  );
};

export default Input;
