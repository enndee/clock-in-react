import React, { Component } from "react";
import openSocket from "socket.io-client";
import List from "./components/list";
import Buttons from "./components/buttons";
import UserMessage from "./components/userMessage";
import MessageInput from "./components/messageInput";
import Loading from "./components/loading";
import ErrorMessage from "./components/errorMessage";
import { serverURL } from "./config.json";

class App extends Component {
  state = {
    list: [],
    selectedItem: "",
    userMessage: "",
    error: false,
    loading: true
  };

  socket = openSocket(serverURL);

  componentDidMount() {
    this.socket.on("populateList", data => this.populateList(data));

    this.socket.on("updateItemOnClient", data => this.handleMessage(data));

    this.socket.on("connect", () => this.setState({ error: false }));

    this.socket.on("connect_error", error => {
      console.log(error);
      this.setState({ loading: false, error: "There is a problem connecting to the server" });
    });
  }

  populateList = data => {
    this.setState({ list: data, loading: false });
  };

  handleMessage = data => {
    this.setState({ list: data, userMessage: "" });

    // If the user has selected an item and another user has updated the message on that item, display the new message
    const { list, selectedItem } = this.state;
    if (selectedItem) {
      const updatedSelectedItem = list.find(({ id }) => id === selectedItem.id);
      const newMessageSelectedItem = { ...this.state.selectedItem };
      newMessageSelectedItem.message = updatedSelectedItem.message;
      this.setState({ selectedItem: newMessageSelectedItem });
    }
  };

  handleItemSelect = item => {
    this.setState({ selectedItem: item });
  };

  handleClockInOut = () => {
    const updatedItem = { ...this.state.selectedItem };
    updatedItem.clockedIn = !updatedItem.clockedIn;
    updatedItem.inOffice = true;
    updatedItem.message = "";
    this.updateItem(updatedItem);
  };

  handleLocationChange = () => {
    const updatedItem = { ...this.state.selectedItem };
    updatedItem.inOffice = !updatedItem.inOffice;
    updatedItem.clockedIn = true;
    this.updateItem(updatedItem);
  };

  updateItem = item => {
    this.socket.emit("updateItemOnServer", item);
    this.setState({ selectedItem: false });
  };

  handleMessageFieldChange = userMessage => {
    this.setState({ userMessage });
  };

  handleMessageFieldSubmit = e => {
    e.preventDefault();

    const { userMessage } = this.state;

    if (userMessage.length) {
      const updatedItem = { ...this.state.selectedItem };
      updatedItem.message = userMessage;
      this.updateItem(updatedItem);
    }
  };

  render() {
    const { selectedItem, userMessage, loading, error } = this.state;
    return (
      <div className="container">
        <div className="heading-background">
          <div className="heading-shape">
            <h1>Clock In</h1>
          </div>
        </div>
        {error && <ErrorMessage errorMessage={error} />}
        {loading ? (
          <Loading />
        ) : (
          <List items={this.state.list} selectedItem={selectedItem} onItemSelect={this.handleItemSelect} />
        )}

        <div className="form-area">
          <Buttons
            onClockInOut={this.handleClockInOut}
            onLocationChange={this.handleLocationChange}
            selectedItem={selectedItem}
          />
          <MessageInput
            selectedUser={selectedItem}
            value={userMessage}
            onMessageFieldChange={this.handleMessageFieldChange}
            onSubmit={this.handleMessageFieldSubmit}
          />
          {selectedItem.message && <UserMessage selectedUser={selectedItem} />}
        </div>
      </div>
    );
  }
}

export default App;
